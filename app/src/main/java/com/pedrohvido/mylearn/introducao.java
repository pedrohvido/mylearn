package com.pedrohvido.mylearn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


/**
 * Created by Alison Orioli on 17/09/2016.
 */
public class introducao extends AppCompatActivity
{
    //Declaração das Variáveis
    protected Button iniciar;
    protected TextView textoInicial;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.introducao);

        //Localizando elementos na Tela
        iniciar = (Button) findViewById(R.id.IniciarId);
        textoInicial = (TextView) findViewById(R.id.textoID);

        iniciar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setContentView(R.layout.activity_main);
            }
        });

        textoInicial.setText
        (
            "Texto Inicial\n\nTestando barra de rolagem na tela!Teste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\nTeste\n"
        );
    }
}
