package com.pedrohvido.mylearn;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AnswerScreen extends Activity {

    public static final String TAG = "WEBSTREAM";
    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView tv4;
    private static int numberRight;
    private int numberWrong;
    private int score;

    public int bkg;  // Holds int identifying background image

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.answerscreen);

        // Choose the background image
        switch(Tema1.subjectIndex){

           // case 0:
           //     bkg = R.drawable.ngc6302_dark;
           //     break;

           // case 1:
             //   bkg = R.drawable.binario;
            //    break;
        }

        // Deal with deprecated methods in setting the background image

        final int sdk = android.os.Build.VERSION.SDK_INT;
        LinearLayout layout =(LinearLayout)findViewById(R.id.LinearLayout1);
        if(sdk < android.os.Build.VERSION_CODES.M) {
            layout.setBackgroundDrawable( getResources().getDrawable(bkg) );
        } else {
            layout.setBackground( getResources().getDrawable(bkg, null) );
        }

        numberRight = Tema1.numberRight;
        numberWrong= Tema1.numberWrong;
        score = (int)(100*Tema1.score);
        String question = Tema1.question;
        boolean isCorrect = Tema1.isCorrect;
        String amplification = Tema1.amplification;

        Log.i(TAG,"right="+numberRight+" wrong="+numberWrong+" score="+score+"%");
        Log.i(TAG,"question="+question);

        Button newButton = (Button) findViewById(R.id.nextQuestion_button);
        newButton.setOnClickListener(event_listener);

        // Questao
        tv1 = (TextView)findViewById(R.id.TextView02);
        tv1.append(question);
        // Respostas
        tv2 = (TextView)findViewById(R.id.TextView03);
        for(int i=0; i<5; i++){
            tv2.append(Tema1.answer[i]+"\n");
        }

        // Correto ou incorreto
        tv3 = (TextView)findViewById(R.id.TextView04);
        String ans = "Your answer "+Tema1.answerArray[Tema1.selectedButton]+" is ";
        if(isCorrect){
            ans += "CORRECT. ";
            ans += "\n\n"+amplification;
        } else {
            ans += "INCORRECT. ";
            ans += " The correct answer is " + Tema1.answerArray[Tema1.correctIndex] +".";
        }
        tv3.append(ans);

        // Score
        tv4 = (TextView)findViewById(R.id.TextView05);
        String s = "Right: "+numberRight+"   Wrong: "+numberWrong+"   Score: " + score +"%";
        tv4.append(s);
    }


    @Override
    protected void onPause(){
        super.onPause();
        // To prevent navigation back to previous question
        finish();
    }

    // Process button clicks
    private OnClickListener event_listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.nextQuestion_button:
                    Tema1.selectedButton = -1;    // So warning is issued if no answer selected
                    Intent i = new Intent(AnswerScreen.this, Tema1.class);  // New question
                    startActivity(i);
                    break;
            }
        }
    };

    // Reseta score
    private void resetScores(){
        numberRight = 0;
        numberWrong = 0;
        score = 0;

        SharedPreferences.Editor edit = Tema1.prefs.edit();
        edit.putInt("numberRight", numberRight);
        edit.putInt("numberWrong", numberWrong);
        edit.putInt("numberQuestions", numberRight+numberWrong);
        edit.putFloat("score", score);
        edit.putInt("qnumber",Tema1.qnumber);
        edit.commit();

        String s = "Right: "+numberRight+"   Wrong: "+numberWrong+"   Score: " + score +"%";
        tv4.setText("");
        tv4.append(s);
    }
}

